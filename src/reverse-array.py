## Reverse an Array in Place

## Reverse Array ##

# Return the number of pairs within a number
def count_pairs(length):
    if length %2 == 0:
        return length/2
    else: 
        return (length-1)/2

# Reverse array in place using one temporary variable
def reverse_array(array):
    length = len(array)
    pairs = count_pairs(length)

    for i in range(pairs):
        temp = array[length-1-i]
        array[length-1-i] = array[i]
        array[i] = temp

    return array

## Make Array ##

# Check if input is a desired integer
def check_input(prompt, error, result):
    try:
        i = int(raw_input(prompt))
    except ValueError:
        print(error)
        return result
    return i

# Retrieve int array from user
def get_array():
    
    ## Select array size
    message = "How long is your array? "
    error = "Must choose an integer. "
    result = -1
    size = check_input(message, error, result)
    while size < 0:
        size = check_array()

    array = list()

    ## Select array elements
    print("Now fill array with integers.")
    for i in range(int(size)):

        message = "Number " + str(i+1) + ": "
        result = None
        element = check_input(message, error, result)
        while element == None:
            element = check_element(i+1)

        array.append(int(element))

    return array

## Main Function ##
def main():
    raw = get_array()
    print("Your array is " + str(raw))
    print("Reversed, it's " + str(reverse_array(raw)))

## Start Program ##
main()
